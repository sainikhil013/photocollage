$(document).ready(function () {
	document.getElementById("clg-grid-container").addEventListener("change", handleFileSelect, false);
	// Rotating Image
	var rotation = 0;
	$('body').on('click', '.btn-rotatecw', function () {
		//Getting the id of the photo container 
		var container_id = $(this).attr('data-layout-class').replace(/rotatecw/gi, "imgdiv");
		rotation = (rotation + 90) % 360;
		$("#" + container_id).find(".draggable_photo").rotate(rotation);
	});
	$('body').on('click', '.btn-rotateccw', function () {
		//Getting the id of the photo container 
		var container_id = $(this).attr('data-layout-class').replace(/rotateccw/gi, "imgdiv");
		rotation = (rotation + 270) % 360;
		$("#" + container_id).find(".draggable_photo").rotate(rotation);
	});
	jQuery.fn.rotate = function (degrees) {
		$(this).css({
			'-webkit-transform': 'rotate(' + degrees + 'deg)',
			'-moz-transform': 'rotate(' + degrees + 'deg)',
			'-ms-transform': 'rotate(' + degrees + 'deg)',
			'transform': 'rotate(' + degrees + 'deg)',

		});
		return $(this);
	};
	//for drag btn  Image
	$('body').on('click', '.btn-filter', function () {
		//Getting the class of the photo container 
		var container_id = $(this).attr('data-layout-class').replace(/rotatecw/gi, "imgdiv");
		console.log(container_id);
		$("#" + container_id).addClass("zoomidx-10");
	});
	//Zooming Image
	$('body').on('click', '.btn-zoomin', function () {
		//Getting the class of the photo container 
		var container_id = $(this).attr('data-layout-class').replace(/rotatecw/gi, "imgdiv");
		var width = $("#" + container_id).find(".draggable_photo").width();
		var height = $("#" + container_id).find(".draggable_photo").height();
		$("#" + container_id).find(".draggable_photo").height(height + 10).width(width + 10);
	});
	$('body').on('click', '.btn-zoomout', function () {
		//Getting the class of the photo container 
		var container_id = $(this).attr('data-layout-class').replace(/rotatecw/gi, "imgdiv");
		var width = $("#" + container_id).find(".draggable_photo").width();
		var height = $("#" + container_id).find(".draggable_photo").height();
		$("#" + container_id).find(".draggable_photo").height(height - 10).width(width - 10);
	});
	//apply changes btn functionality
	$('body').on('click', '.btn-apply', function () {
		//Getting the class of the photo container 
		var container_id = $(this).attr('data-layout-class');
		var settings_id = $(this).attr('data-layout-class').replace(/imgdiv/gi, "settings");
		var applyChanges_id = $(this).attr('data-layout-class').replace(/imgdiv/gi, "applyChanges");
		document.getElementById(settings_id).style.display = "none";
		$("#" + container_id).find(".edit-btn-div").show();
		$(".btn-preview").removeAttr('disabled');
	});
	//edit btn click functionality
	$('body').on('click', '.btn-edt', function () {
		//Getting the class of the photo container 
		var container_id = $(this).attr('data-layout-class');
		var settings_id = $(this).attr('data-layout-class').replace(/imgdiv/gi, "settings");
	//	console.log(settings_id);
		document.getElementById(settings_id).style.display = "block";
		$("#" + container_id).find(".edit-btn-div").hide();
		$(".btn-preview").attr('disabled', 'disabled');
	});
	//get preview Image


	var getCanvas; // global variable
	$(".btn-preview").on('click', function () {
		$(".close_icon,.btn-filter,.rotatecw_icon,.rotateccw_icon,.zoomin_icon,.zoomout_icon,.img-center-div").css("display", "none");
		$(".over-flow-hidden").css("overflow", "unset");
		html2canvas(document.querySelector("#photo-grid")).then(canvas => {


			$("#previewImage").html(canvas);
			getCanvas = canvas;
			$(".close_icon,.btn-filter,.rotatecw_icon,.rotateccw_icon,.zoomin_icon,.zoomout_icon,.img-center-div").css("display", "block");
			$(".over-flow-hidden").css("overflow", "hidden");
		});

		$(".btn-download").show();
	});


	// downloading image...
	$(".btn-download").on('click', function () {

		try {
			var imgageData = getCanvas.toDataURL("image/png");
			//	Now browser starts downloading it instead of just showing it
			var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
			$("#btn-download-college-grid").attr("download", "photogrid.png").attr("href", newData);
		} catch (error) {
			console.log(error);

		}



	});




	//resetting
	$(".btn-reset").on('click', function () {

		for (let i = 1; i <= 3; i++) {
			
			deletePhoto(i);
		}
		$("#previewImage").html(" ");
		$(".btn-download").hide();
		$(".btn-preview").hide();
		$(".btn-reset").hide();
	});



	// mouse  down , move , and up events trigger functionality
	var isdraging = false;
	$(".button_container").on('mousedown mousemove  mouseup ', function mouseState(e) {
		var container_id = $(this).attr('data-layout-class');
		if (e.type == "mousedown") {
			//console.log("mouse down   ", container_id);
			$("#" + container_id).css("cursor", "pointer");
			isdraging = true;
			this.mouseStartingPositionx = Number(e.pageX);
			this.mouseStartingPositiony = Number(e.pageY);
			this.pos = $("#" + container_id).find(".draggable").position(); // returns an object with the attribute top and left
			this.pos.top;  // top offset position
			this.pos.left;

			this.imageStartingpositionTop = Number(this.pos.top);
			this.imageStartingpositionLeft = Number(this.pos.left);
			console.log(this.pos.top, this.pos.left);


		}
		if (e.type == "mousemove") {
			//code triggers on hold
			if (isdraging) {
				console.log(" mouse is  draging", container_id);
				$("#" + container_id).css("cursor", "move");
				this.mouseCurrentPositionx = Number(e.pageX);
				this.mouseCurrentPositiony = Number(e.pageY);
				//console.log(this.mouseCurrentPositionx ,this.mouseCurrentPositiony );

				this.xDiff = Number(this.mouseCurrentPositionx - this.mouseStartingPositionx);
				this.yDiff = Number(this.mouseCurrentPositiony - this.mouseStartingPositiony);
				console.log(this.xDiff, this.yDiff);
				this.x = this.xDiff + this.imageStartingpositionLeft;
				this.y = this.yDiff + this.imageStartingpositionTop;

				this.pos = $("#" + container_id).find(".draggable").css({ 'left': this.x + 'px', 'top': this.y + 'px', 'position': 'relative', })

				// we need to drag the image now	
			}
		}
		if (e.type == "mouseup") {
			console.log(" mouse up", container_id);
			$("#" + container_id).css("cursor", "default");
			isdraging = false;
		}
	});
	
})


//function handleFileSelect(event) i.e imge upload funtionality...
function handleFileSelect(event) {


	//Getting the id of the clicked file input
	if (event.target !== event.currentTarget) {
		var clicked_input = event.target.id;
	}
	//Stopping the propagation at the parent element just to avoid having to deal with the event running up and down the DOM.
	//event.stopPropagation();
	var files = event.target.files;

	// validating size of the image....

	if (files[0].size / 1024 / 1024 > 10) {

		alert(' Please uplode image size less than 10MB ,Your selected image size is ' + files[0].size / 1024 / 1024 + "MB ");

		return;
	}



	// Loop through the FileList and render image files.	
	for (var i = 0, f; f = files[i]; i++) {
		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}
		//From the clicked input, we form the name of the container we will load the  photo in by replaciong the string "files" with "photo"
		var container_id = clicked_input.replace(/fileinput/gi, "output");
		var img_id = clicked_input.replace(/fileinput/gi, "img");
		var imgdragdiv_id = clicked_input.replace(/fileinput/gi, "imgdragdiv");
		var settings_id = clicked_input.replace(/fileinput/gi, "settings");
		var fileinput_id = clicked_input.replace(/fileinput/gi, "fileinput");
		var imgdiv_id = clicked_input.replace(/fileinput/gi, "imgdiv");



// Hiding file input btn and showing loder
		$("#" + imgdiv_id).find(".file-input-div").hide();
		$("#" + imgdiv_id).find(".loader").show();

		var reader = new FileReader();
		// Closure to capture the file information.
		reader.onload = (function (file) {
			return function (e) {
				var image = new Image();
				image.origin = 'anonymous';
				image.src = e.target.result;
				image.onload = function () {
					// access image size here 
					var canvas = document.createElement("canvas");
					var ctx = canvas.getContext("2d");
					ctx.drawImage(image, 0, 0);
					var MAX_WIDTH = $("#" + imgdiv_id).width();
					var MAX_HEIGHT = $("#" + imgdiv_id).height();
					var width = this.width;
					var height = this.height;
					//console.log('uploded imgae width is', MAX_WIDTH);
					//console.log('uploded imgae height is', MAX_HEIGHT);
					if (width > height) {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					} else {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					}
					canvas.width = width;
					canvas.height = height;
					var ctx = canvas.getContext("2d");
					ctx.drawImage(image, 0, 0, width, height);
					var dataurl = canvas.toDataURL("image/png");
					var div = document.createElement('div');
					div.innerHTML = ['<div id="' + imgdragdiv_id + '"  ><img crossOrigin="Anonymous"  id="' + img_id + '" data-filter="" class="draggable_photo draggable ui-widget-content " src="' + dataurl + '" title="', escape(file.name), '"/></div>'].join('');
					document.getElementById(container_id).insertBefore(div, null);

			// hinding loaer and showing edit button
					$("#" + imgdiv_id).find(".loader").hide();
					$("#" + imgdiv_id).find(".edit-btn-div").show();
					$("#" + imgdragdiv_id).draggable(

						{
							handle: "#settings_1"
						}
					);
					//	$("#" + imgdiv_id).find(".draggable").css({ 'transform': 'scale(0.5)' });
					$("#" + imgdiv_id).find(".draggable").draggable({
						start: function () {

						},
						drag: function () {
							//event when img draging 




						},
						stop: function () {

							// event when img draging stoped
						}
					});
				};
			};
		})(f);
		// Read in the image file as a data URL.

		reader.readAsDataURL(f);
	}
	$(".btn-preview").show();
	$(".btn-reset").show();


}

//delete photo functionality

function deletePhoto(container_id) {
	//alert("delete photo");
	//console.log(container_id);
	$("#output_" + container_id).html('');
	//Restoring the input value to "No file chosen"
	$("#fileinput_" + container_id).val('');
	//making disply none for settings div
	document.getElementById("settings_" + container_id).style.display = "none";
	//enableing input file
	$("#imgdiv_" + container_id).find(".file-input-div").show();
	$("#imgdiv_" + container_id).find(".edit-btn-div").hide();
}
