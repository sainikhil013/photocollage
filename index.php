<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="normalize.min.css">
	<link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css">
	<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="styles.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"
		integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="./scripts/html2canvas.js"></script>
	<script src="./scripts/scripts.js"></script>

</head>

<body>


	<div class="pop-full " role="document">
		<div class="modal-header">
			<h2 class="modal-title">College Maker</h2>
		</div>
		<div class="modal-body  ">
			<div class="row  ">
				<div class="col-lg-12 col-md-12 col-sm-12  ">
					<div class="card m-portlet--full-height">
						<div class="portlet ">
							<div class="portlet-title">
								Grid Template

							</div>

							<div class="portlet-body row ">
								<div class="col-lg-6 col-md-12 col-sm-12">

									<div class="clg-gridd" id="clg-grid-container">
										<!--  charan -->
										<div id="photo-grid" class="clg-grid  over-flow-hidden" width="600px"
											height="400px" style="background-color:#f1f1f1f1">
											<img src="./img/banner.png" alt="Selected Image" width="600px"
												height="400px" class="img-mask" >
											<!-- imagae div 1 -->
											<div id="imgdiv_1"
												style="width:232px;height:176px;position:absolute;left:25px;top:25px;background-color: #fff;"
												class="over-flow-hidden ">

												<div class="img-center-div" style="display:block">

													<div class="file-input-div" style="display:block">
														<input type="file" id="fileinput_1"
															class=" btn-fileinput btn btn primary  " accept="image/*"
															style="display:none">
														<label for="fileinput_1" class="btn btn-primary"> <span><i
																	class="fa fa-cloud-upload"
																	aria-hidden="true"></i></span>
														</label>
													</div>

													<div class="edit-btn-div" style="display:none">
														<button id="edit_1" type="button"
															class="btn btn-primary btn-xs  btn-edt "
															data-layout-class="imgdiv_1">
															<span>
																<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
															</span>
														</button>
													</div>
													<div class="loader" style="display:none">
															<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
													</div>

												</div>

												<div id="settings_1" class="button_container " style="display:none"
													data-layout-class="imgdiv_1">

													<button id="close_1" type="button"
														class="btn btn-danger btn-xs close_icon" style="top:0px;left:0"
														onclick="deletePhoto(1)"><span>
															<i class="fa fa-trash"
																aria-hidden="true"></i></span></button>

													<button id="rotatecw_1" type="button"
														class="btn btn-primary btn-xs rotatecw_icon btn-rotatecw "
														style="top:30px;left:0" data-layout-class="imgdiv_1"><span> <i
																class="fa fa-rotate-right" aria-hidden="true"></i>
														</span></button>
													<button id="rotateccw_1" type="button"
														class="btn btn-primary btn-xs rotateccw_icon btn-rotateccw "
														style="top:60px;left:0" data-layout-class="imgdiv_1"><span> <i
																class="fa fa-rotate-left"
																aria-hidden="true"></i></span></button>
													<button id="zoomin_1" type="button"
														class="btn btn-primary btn-xs zoomin_icon btn-zoomin "
														style="top:90px;left:0" data-layout-class="imgdiv_1"><span> <i
																class="fa fa-search-plus"
																aria-hidden="true"></i></span></button>
													<button id="zoomout_1" type="button"
														class="btn btn-primary btn-xs zoomout_icon btn-zoomout "
														style="top:120px;left:0" data-layout-class="imgdiv_1"><span> <i
																class="fa fa-search-minus"
																aria-hidden="true"></i></span></button>

													<button id="applyChanges_1" type="button"
														class="btn btn-primary btn-xs  btn-apply "
														style="bottom:0px;left:45%"
														data-layout-class="imgdiv_1"><span>Apply</span></button>
												</div>

												<div class="output-div">

													<output id="output_1">

													</output>
												</div>
												

											</div>

											<!-- imagae div 2 -->
											<div id="imgdiv_2"
												style="width:232px;height:176px;position:absolute;left:340px;top:204px;background-color: #fff;"
												class="over-flow-hidden">


												<div class="img-center-div">

													<div class="file-input-div" style="display:block">
														<input type="file" id="fileinput_2"
															class=" btn-fileinput btn btn primary  " accept="image/*"
															style="display:none">
														<label for="fileinput_2" class="btn btn-primary"> <span><i
																	class="fa fa-cloud-upload"
																	aria-hidden="true"></i></span>
														</label>
													</div>

													<div class="edit-btn-div" style="display:none">
														<button id="edit_2" type="button"
															class="btn btn-primary btn-xs  btn-edt "
															data-layout-class="imgdiv_2">
															<span>
																<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
															</span>
														</button>
													</div>

													<div class="loader" style="display:none">
															<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
													</div>


												</div>

												<div id="settings_2" class="button_container" style="display: none;"
													data-layout-class="imgdiv_2">

													<button id="close_2" type="button"
														class="btn btn-danger btn-xs close_icon" style="top:0px;right:0"
														onclick="deletePhoto(2)"><span>
															<i class="fa fa-trash"
																aria-hidden="true"></i></span></button>

													<button id="rotatecw_2" type="button"
														class="btn btn-primary btn-xs rotatecw_icon btn-rotatecw "
														style="top:30px;right:0" data-layout-class="imgdiv_2"><span> <i
																class="fa fa-rotate-right" aria-hidden="true"></i>
														</span></button>
													<button id="rotateccw_2" type="button"
														class="btn btn-primary btn-xs rotateccw_icon btn-rotateccw "
														style="top:60px;right:0" data-layout-class="imgdiv_2"><span> <i
																class="fa fa-rotate-left"
																aria-hidden="true"></i></span></button>
													<button id="zoomin_2" type="button"
														class="btn btn-primary btn-xs zoomin_icon btn-zoomin "
														style="top:90px;right:0" data-layout-class="imgdiv_2"><span> <i
																class="fa fa-search-plus"
																aria-hidden="true"></i></span></button>
													<button id="zoomout_2" type="button"
														class="btn btn-primary btn-xs zoomout_icon btn-zoomout "
														style="top:120px;right:0" data-layout-class="imgdiv_2"><span> <i
																class="fa fa-search-minus"
																aria-hidden="true"></i></span></button>
													<button id="applyChanges_2" type="button"
														class="btn btn-primary btn-xs  btn-apply "
														style="bottom:0px;left:45%"
														data-layout-class="imgdiv_2"><span>Apply</span></button>

												</div>

												<div class="output-div">
													<output id="output_2">

													</output>
												</div>



											</div>
										</div>
										<!-- charan -->





									</div>


								</div>
								<div class="col-lg-6 col-md-12 col-sm-12">

									<button id="btn-Preview-Image"
										class="btn btn-sucess  btn-preview  m-t-10 m-r-10 m-b-10"> Get Preview</button>
									<button id="reset"
										class="btn btn-danger   btn-reset  m-t-10 m-r-10 m-b-10 ">Reset</button>


									<div id="previewImage" class="img-preview">

									</div>
									<div class="pull-right">

										<button class="btn btn-sucess   btn-download  m-t-10 m-r-10"><a
												id="btn-download-college-grid" href="#">Download</a></button>



									</div>





								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>




</body>

</html>